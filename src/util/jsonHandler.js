require("dotenv").config();

// None of these need to be npm installed as they come with Node.js.
import util from "util";
import fs from "fs";
import path from "path";

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

// Defines the path for the database file `items.json`.
const catsDB = path.resolve(`${process.env.catsDbRoute}`);

async function readCats() {
  const json = await readFile(catsDB);
  return JSON.parse(json);
}

async function writeCats(items) {
  // The `null` and `2` here are so the JSON is formatted in a readable format, it's not required.
  const json = JSON.stringify(items, null, 2);

  // We return the promise here so the promise returned by `writeItems` doesn't resolve until
  // all the items are actually written.
  return writeFile(catsDB, json);
}

export { readCats, writeCats };
